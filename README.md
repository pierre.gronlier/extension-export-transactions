 # Web extension to export transactions and receipts from indy.fr

This extension is close to webscrapping, hence not stable.

The proper way would be to have access to an official API.

In the meantime, it works ... :)

![](docs/screenshot-menu.png)

![](docs/screenshot-export.png)

## build

```shell
npx gulp
cd dist/
web-ext build
```

## Local dev

```shell
$ npx gulp watch
```

```shell
$ cd dist/
$ web-ext run --devtools
```
