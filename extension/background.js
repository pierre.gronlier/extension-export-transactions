// https://gist.github.com/72lions/4528834
var appendBuffer = function (buffer1, buffer2) {
    var tmp = new Uint8Array(buffer1.byteLength + buffer2.byteLength);
    tmp.set(new Uint8Array(buffer1), 0);
    tmp.set(new Uint8Array(buffer2), buffer1.byteLength);
    return tmp.buffer;
};

// ADD CONTEXT MENU ENTRY

browser.contextMenus.create({
    id: "export-transactions",
    title: "Indy: Export all transactions",
    "documentUrlPatterns": ["https://app.indy.fr/transactions"]
});

browser.contextMenus.onClicked.addListener((info, tab) => {
    if (info.menuItemId === "export-transactions") {
        browser.tabs.executeScript({
            file: "content.js"
        });
    }
});

// KEEP TRACK OF LOADED TRANSACTIONS

let gTransactions = [];

function captureTransactions(details) {
    console.log(`${details.requestId}: ${details.url}`);
    let filter = browser.webRequest.filterResponseData(details.requestId);
    let data = new ArrayBuffer(0);
    let url = new URL(details.url);
    let page = url.searchParams.get("page") || 1;

    filter.ondata = (event) => {
        console.log(`${details.requestId}: received ${event.data.byteLength} bytes`);
        filter.write(event.data);
        data = appendBuffer(data, event.data);
    }
    filter.onstop = (event) => {
        console.log(`${details.requestId}: stopped with ${data.byteLength} bytes`);
        filter.disconnect();
        let decoder = new TextDecoder("utf-8");
        data = decoder.decode(data);
        data = JSON.parse(data);
        if (page == 1) { // we are on the first page
            gTransactions = data.transactions;
        } else { // we loaded more page
            gTransactions = [...gTransactions, ...data.transactions]; // spread operator
        }
        console.log(`total transactions: ${gTransactions.length}`);
    };
}

browser.webRequest.onBeforeRequest.addListener(captureTransactions,
    { urls: ["https://app.indy.fr/api/transactions/transactions-list?*"] }, ["blocking"]
);

// KEEP TRACK OF API HEADERS

let gApiHeaders = [];

function captureHeaders(details) {
    console.log(`${details.requestId}: ${details.url}`);
    gApiHeaders = details.requestHeaders;
}

browser.webRequest.onBeforeSendHeaders.addListener(captureHeaders,
    { urls: ["https://app.indy.fr/api/transactions*"] }, ["requestHeaders"]
);

// LISTEN TO CONTENT SCRIPT MESSAGE

async function handleMessage(message) {
    if (message.action == "get-transactions") {
        console.log(`found ${gTransactions.length} transactions`);
        return { transactions: gTransactions, apiHeaders: gApiHeaders};
    } else {
        console.log(`content script sent a message: ${message.action}`);
    }
    return { err: `unknown action: ${message.action}` };
}

browser.runtime.onMessage.addListener(async (message, sender, sendResponse) => {
    const response = await handleMessage(message);
    return response;
});
