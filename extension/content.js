import * as JSZip from 'jszip';

console.log(`has JSZip uint8array support: ${JSZip.support.uint8array}`);

// progress bar
// https://www.w3schools.com/w3css/w3css_progressbar.asp

// export to CSV https://stackoverflow.com/questions/14964035/how-to-export-javascript-array-info-to-csv-on-client-side
// TODO: could be replaced by https://github.com/eligrey/FileSaver.js
function downloadBlob(content, filename, contentType) {
    // Create a blob
    var blob = new Blob([content], { type: contentType });
    var url = URL.createObjectURL(blob);

    // Create a link to download it
    var pom = document.createElement('a');
    pom.href = url;
    pom.setAttribute('download', filename);
    pom.click();
}

async function listReceipts(transactions, apiHeaders) {
    let countReceipts = 0;
    for (let i = 0; i < transactions.length; ++i) {
        console.log(`list receipts: ${Math.floor(i * 100 / transactions.length)}%`);
        if (transactions[i].receipts.length == 0) {
            continue;
        }
        let url = new URL("https://app.indy.fr/api/receipts");
        for (const receipt of transactions[i].receipts) {
            url.searchParams.append("receiptIds[]", receipt);
        }
        url.searchParams.append("transactionId", transactions[i]._id);
        const response = await fetch(url, { headers: apiHeaders });
        const data = await response.json();
        transactions[i].receipts = data.receipts;
        countReceipts += data.receipts.length;
    }
    console.log(`found ${countReceipts} receipts`);
    return transactions;
}

async function convertToCSV(transactions) {
    let csvRows = [];
    const headers = ["date", "description", "id", "amount", "annotation"];
    csvRows.push(headers.join(','));
    for (const transaction of transactions) {
        const values = [transaction.isodate,
            transaction.description,
            transaction._id,
            transaction.amountInCents / 100,
            transaction.annotation ?? ""
        ]
        csvRows.push(values.join(','));
    }
    // Returning the array joining with new line
    return csvRows.join('\n')
}

async function getTransactions(message) {
    // prepapre Headers from last captured API call
    let apiHeaders = new Headers();
    for (const header of message.apiHeaders) {
        apiHeaders.append(header.name, header.value);
    }
    const transactions = await listReceipts(message.transactions, apiHeaders);
    console.log(transactions);

    let exportZip = new JSZip();
    exportZip.file("transactions.json", JSON.stringify(transactions));
    exportZip.file("transactions.csv", await convertToCSV(transactions));

    for (let i = 0; i < transactions.length; ++i) {
        console.log(`download receipt: ${Math.floor(i * 100 / transactions.length)}%`);
        const transaction = transactions[i];
        if (transaction.receipts.length == 0) {
            continue;
        }
        let folderName = `${transaction.isodate} - ${transaction.description} - ${transaction._id}`
        folderName = folderName.replace(/[^-a-zA-Z0-9]/gi, ' '); // sanatize from non alphanumerical character
        let subfolder = exportZip.folder(folderName);

        if (transaction.annotation !== undefined && transaction.annotation.length != 0) {
            subfolder.file("annotation.txt", transaction.annotation);
        }
        for (let receipt of transaction.receipts) {
            if (receipt.is_deleted) {
                continue;
            }
            let receiptName = receipt.key.replace(/^.*[\\\/]/, ''); // filename with extension
            receiptName = receiptName.replace(/\.[^/.]+$/, "") // remove extension
            receiptName = receiptName.replace(/[^-a-zA-Z0-9]/gi, ' '); // sanatize from non alphanumerical character
            receiptName = `${receiptName}.pdf`;
            console.log(receipt.pdf);
            let response = await fetch(receipt.pdf, { mode: 'cors' });
            subfolder.file(receiptName, await response.arrayBuffer());
        }
    }
    let zipContent = await exportZip.generateAsync({ type: "blob" });
    return zipContent;
}

async function getLoadedTransaction() {
    const response = await browser.runtime.sendMessage({
        action: "get-transactions",
    });
    const zipContent = await getTransactions(response);
    downloadBlob(zipContent, 'export.zip', 'application/zip')
}

getLoadedTransaction();
