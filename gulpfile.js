const gulp = require('gulp');
const webpack = require('webpack-stream');

gulp.task('contentjs', function () {
    return gulp.src(['extension/content.js'])
        .pipe(webpack({
            mode: (process.env.NODE_ENV || false) ? 'production' : 'development',
            output: {
                filename: './content.js'
            }
        }))
        .pipe(gulp.dest('./dist/'))
});

gulp.task('assets', function () {
    return gulp.src(['extension/background.js', 'extension/manifest.json'])
        .pipe(gulp.dest('dist/'));
});

gulp.task('default', gulp.series('contentjs', 'assets'));

gulp.task('watch', function () {
    gulp.watch('extension/*', gulp.series(['default']));
});
